# 변수 설명
- JAVA_HOME : /usr/lib/jvm/java-7-openjdk-amd64/
> 자바 설치된 경로
- PROJECT_PATH : /var/lib/jenkins/workspace/moneybook-deploy
> 프로젝트가 설치된 경로..
- MODULE : moneybook
> 모듈 이름
- DOMAIN : moneybook.kr
> 도메인 이름


# 사용자 권한 설정

```
$ sudo visudo
```

jennkins 부분에 root(sudo) 권한을 요청할때 패스워드 요청 없이 설치 할수 있도록
설정 한다.

**jenkins    ALL=NOPASSWD: ALL**

```
# This file MUST be edited with the 'visudo' command as root.
#
# Please consider adding local content in /etc/sudoers.d/ instead of
# directly modifying this file.
#
# See the man page for details on how to write a sudoers file.
#
Defaults        env_reset
Defaults        secure_path="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"

# Host alias specification

# User alias specification

# Cmnd alias specification

# User privilege specification
root    ALL=(ALL:ALL) ALL
jenkins    ALL=NOPASSWD: ALL

# Members of the admin group may gain root privileges
%admin ALL=(ALL) ALL

# Allow members of group sudo to execute any command
%sudo   ALL=(ALL:ALL) ALL

# See sudoers(5) for more information on "#include" directives:
```


# Dependency
- [ubuntu-12.04](https://hub.docker.com/r/mrlshjjang/ubuntu-12.04/)
- [django-server](https://hub.docker.com/r/mrlshjjang/django-server/)

# 참고
[jenkins original](https://github.com/jenkinsci/docker)
